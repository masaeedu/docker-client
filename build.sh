#! /usr/bin/env sh
docker run --rm -v ${PWD}:/src docker:git sh -c "cd /src && git submodule sync --recursive && git submodule update --init --recursive"
docker run --rm \
  -w /gen \
  -e GEN_DIR=/gen \
  -e MAVEN_CONFIG=/var/maven/.m2 \
  -u "$(id -u):$(id -g)" \
  -v "${PWD}/package:/package" \
  -v "${PWD}/vendor/moby:/moby" \
  -v "${PWD}/vendor/swagger-codegen:/gen" \
  -v /tmp/maven:/var/maven/.m2/repository \
  --entrypoint /gen/docker-entrypoint.sh \
  maven:3-jdk-7 \
  generate -i /moby/api/swagger.yaml -l typescript-fetch -DmodelPropertyNaming=original -o /package
cd package && chmod +x ./postprocess.sh && ./postprocess.sh && cd -
cp ./README.md package
docker run --rm -e "NPM_CONFIG_UNSAFE_PERM=true" -v ${PWD}/package:/package node bash -c "cd /package && npm install && npm run doc && npm pack"
